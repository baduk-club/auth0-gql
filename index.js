const { ApolloServer } = require("apollo-server");
const gql = require("graphql-tag");
const jwt = require("jsonwebtoken");
const fetch = require("node-fetch");

const typeDefs = gql`
  type auth0_profile {
    email: String
    picture: String
  }

  type Query {
    auth0(id: ID): auth0_profile
  }
`;

let cached_token;
let expiry_time;

function getAuth0AccessToken() {
  const params = new URLSearchParams();

  Object.entries({
    grant_type: "client_credentials",
    client_id: process.env.AUTH0_CLIENT_ID,
    client_secret: process.env.AUTH0_CLIENT_SECRET,
    audience: "https://" + process.env.AUTH0_DOMAIN + "/api/v2/"
  }).forEach(([key, value]) => params.append(key, value));

  return fetch("https://" + process.env.AUTH0_DOMAIN + "/oauth/token", {
    method: "POST",
    body: params
  })
    .then(res => res.json())
    .catch(e => console.log(e));
}

async function refreshCache(time) {
  console.log("Token is expired. Refreshing cache...");
  const { access_token, expires_in } = await getAuth0AccessToken();
  cached_token = access_token;
  expiry_time = time + expires_in;
}

async function fetchToken() {
  const now = new Date().getTime() / 1000;
  // Token has expired
  if (!expiry_time || expiry_time < now) {
    await refreshCache(now);
  }
  return cached_token;
}

async function getProfileInfo(user_id) {
  let token = await fetchToken();
  const headers = { Authorization: "Bearer " + token };
  return fetch(
    "https://" + process.env.AUTH0_DOMAIN + "/api/v2/users/" + user_id,
    { headers: headers }
  ).then(response => response.json());
}

function userId(args, context) {
  if (args.id) return args.id;
  try {
    const authHeaders = context.headers.authorization;
    const token = authHeaders.replace("Bearer ", "");
    const decoded = jwt.decode(token);
    return decoded.sub;
  } catch (e) {
    console.log(e);
    return null;
  }
}

const resolvers = {
  Query: {
    auth0: (parent, args, context) => {
      // read the authorization header sent from the client
      const user_id = userId(args, context);
      if (!user_id) return null;
      return getProfileInfo(user_id).then(function(resp) {
        if (!resp) {
          console.log(resp);
          return null;
        }
        return { email: resp.email, picture: resp.picture };
      });
    }
  }
};

const context = ({ req }) => {
  return { headers: req.headers };
};

const schema = new ApolloServer({ typeDefs, resolvers, context });

schema.listen({ port: process.env.PORT }).then(({ url }) => {
  console.log(`schema ready at ${url}`);
});
